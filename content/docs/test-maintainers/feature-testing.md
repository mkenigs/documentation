---
title: Kernel Features and Matching Tests
linkTitle: Kernel Features + Matching Tests 
description: Process to ensure all kernel features include matching tests
weight: 40

---

To ensure there is no breakage, every kernel feature should have a mapping
testcase. CKI can trigger targeted testing against patches and builds. Bugzilla
keywords are used to track test coverage. Bugzilla assignees can assign the
following test coverage keywords for bugs which contain the `FutureFeature`
keyword.

## TestCaseNeeded

* See [Contributing Tests] for steps to onboard a new testcase to CKI.
* If the `TestCaseNeeded` keyword is added, please include the suggested test
  steps in a Bugzilla comment. It is possible that an existing test can be
  updated to verify this feature or target additional hardware.
* If the `OtherQA` keyword is added, please ping the partner to check if test
  coverage can be added to verify the feature. It is not required to add
  `TestCase[Not]Needed` keywords in this case.

## TestCaseNotNeeded

* If the `TestCaseNotNeeded` keyword is added, please add the justification in
  a Bugzilla comment

## TestCaseProvided

* An existing test case exists, please add a Bugzilla comment with a link if
  the test source is public

[Contributing Tests]: onboarding.md
