---
title: "Pipeline dataflow"
description: >
  Diagram of the dataflow throughout the pipeline
---

![Triggers dataflow](pipeline-dataflow-triggers.png "Triggers dataflow")
![Results dataflow](pipeline-dataflow-results.png "Results dataflow")
