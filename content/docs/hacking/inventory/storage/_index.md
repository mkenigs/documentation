---
title: "Storage"
description: Block and object storage
layout: inventory-table
weight: 40
---

For storage, the CKI setup uses both NFS shares and S3 buckets.
